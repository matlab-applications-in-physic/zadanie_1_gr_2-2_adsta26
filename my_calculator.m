%7.1
disp('ex. 7.1') %display exercise number 
h = 6.62607015e-34 %Planck constant from https://physics.nist.gov/cgi-bin/cuu/Value?h|search_for=h
h/(2*pi) %value of h/2?, where h - Planck constant
pause %wait for keypress

%7.2
disp('ex. 7.2') %display exercise number 
sind(30/exp(1)) %value of sin(30�/e), where e - Euler's number
pause %wait for keypress

%7.3
disp('ex. 7.3') %display exercise number 
hex2dec('00123d3')/2.455e23 %value of 0x00123d3/(2,455�10^23). 
                            %hex2dec function convert hexadecimal value to decimal value
pause %wait for keypress

%7.4
disp('ex. 7.4') %display exercise number 
sqrt(exp(1)-pi) %value of ?(e??), where e - Euler's number
pause %wait for keypress

%7.5
disp('ex. 7.5') %display exercise number 
num2str(pi, 11) %display ? with 10 numbers after decimal point
                %num2str(a,n) function convert decimal number a to string contain n chars
pause %wait for keypress

%7.6
disp('ex. 7.6') %display exercise number 
datenum(date) - datenum('26-jul-1998') %Number of days between 26.07.1998 and 18.10.2019. 
                                       %datenum function return a numbers of day between January 0, 0000 and date passed in argument 
                                       %date function return current date
pause %wait for keypress

%7.7
disp('ex. 7.7') %display exercise number 
Rz = 6378.137 %Equatorial radius of Earth from https://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html
atan(exp((sqrt(7)/2) - log(Rz/10^5))/hex2dec('aabb')) % value of arctg((e^((?7 / 2) - (ln(R/10^5))))/(0xaabb)),
                                                       % where R - Equatorial radius of Earth, e - Euler's number
                                                       %hex2dec function convert hexadecimal value to decimal value
pause %wait for keypress

%7.8
disp('ex. 7.8') %display exercise number 
N_A = 6.02214076e23 %Avogadro constant from https://physics.nist.gov/cgi-bin/cuu/Value?na|search_for=Na
N_A * 0.2e-6 %Number of particles in 0.2umol Ethanol
pause %wait for keypress

%7.9
disp('ex. 7.9') %display exercise number 
C12 = ans * 2/9 %Number of C12 particles in 0.2umol Ethanol
C13 = C12 / 101 %Number of C13 particles in 0.2umol Ethanol
(C13/ans)*1000 %Per mil of C13 particles in Ethanol
